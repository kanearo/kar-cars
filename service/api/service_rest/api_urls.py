from django.urls import path

from .api_views import api_list_services, api_list_technician, service_history_list

urlpatterns = [
    path("services/", api_list_services, name="api_list_services"),
    path("technicians/", api_list_technician, name="api_list_technician"),
    path("history/", service_history_list, name="service_history_list")
]
