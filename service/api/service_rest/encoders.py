from common.json import ModelEncoder
from .models import AutomobileVO, Service, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id"
    ]


class ServiceListEncoder(ModelEncoder):
    model = Service
    properties = [
        "id"
        "VIN",
        "customer_name",
        "appointment_time",
        "technician",
        "reason",
        "finished"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "name"
    ]


class ServiceDetailEncoder(ModelEncoder):
    model = Service
    properties = [
        "VIN",
        "customer_name",
        "appointment_time",
        "technician",
        "reason",
    ]
