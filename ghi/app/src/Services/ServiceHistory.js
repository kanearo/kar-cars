import React, { useState, useEffect } from "react";


const ServiceHistory = () => {
    const[ServiceHistory, setServiceHistory] = useState([]);
    const fetchData = async () => {
      const url = "http://localhost:8100/api/service_history/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setServiceHistory(data)
        }
      };

      useEffect(() => {
        fetchData();
      }, []);

      const handleDelete = async (id) => {
        const url = "http://localhost:8100/api/service_history/${id}/";
        const fetchConfig = {
          method: "delete",
          headers: {
            "Content-Type": "application/json",
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          fetchData();
        } else {
          alert("services was not detected!");
        }
      };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>customer_name</th>
                    <th>appointment_time</th>
                    <th>Technician_name</th>
                    <th>Reason</th>
                </tr>
            </thead>
            {/* <tbody>
                {this.state.ServiceAppointment.map(services =>{
                  return(
                    <tr key={services.VIN }>
                        <td>{ services.customer_name }</td>
                        <td>{ services.appointment_time }</td>
                        <td>{ services.Technician }</td>
                        <td>{ services.Reason }</td>
                    </tr>
                  );
                })}
            </tbody> */}
        </table>
    )
            }
export default ServiceHistory;
