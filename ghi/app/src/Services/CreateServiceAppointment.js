import React, { useState, useEffect } from "react";

function ServiceAppointment(props) {
    const[appointment, setAppointment] = useState([]);

    const[VIN, setAppointmentVIN] = useState(" ");
    const handleAppointmentVINChange = (event) => {
        const value = event.target.value;
        setAppointmentVIN(value)
    }

    const[customer_name, setName] = useState(" ");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const[appointment_time, setDate] = useState(" ");
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value)
    }

    const[technician, setTechnician] = useState(" ");
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value)
    }

    const[reason, setReason] = useState(" ");
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value)
    }

    const fetchData = async () => {
    const url = "http://localhost:8080/api/services/";

    const response = await fetch (url);


        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAppointment(data.appointment)
        }
    }
        useEffect(() => {
            fetchData();
        }, []);

        const handleSubmit = async (event) => {
            event.preventDefault();
            const data = {};
            data.VIN = VIN;
            data.customer_name = customer_name;
            data.appointment_time = appointment_time;
            data.technician = technician;
            data.reason = reason;


        const appointmentUrl = "http://localhost:8080/api/services/";

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };
        console.log(fetchConfig)
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);
        };
            setAppointment(" ");
            setName(" ");
            setDate(" ");
            setTechnician(" ");
            setReason(" ");
        }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new service appointment</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input value={VIN} onChange={handleAppointmentVINChange} placeholder="VIN" required type="text" name="VIN" id="VIN" className="form-control" />
                  <label htmlFor="name">VIN of Vehicle</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={customer_name} onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="room_count">customer_name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={appointment_time} onChange={handleDateChange} placeholder="date" required type="datetime-local" name="date" id="date" className="form-control" />
                  <label htmlFor="room_count">Appointment</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={technician} onChange={handleTechnicianChange} placeholder="technician" required type="text" name="technician" id="technician" className="form-control" />
                  <label htmlFor="room_count">Assigned Technician</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={reason} onChange={handleReasonChange} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                  <label htmlFor="room_count">Reason</label>
                </div>

                {/* <div className="mb-3">
                  <select value={state} onChange={handleStateChange} required name="state" id="state" className="form-select">
                    <option value="">Choose a location</option>
                    {technician.map(technician => {
                      return (
                        <option key={technician.abbreviation} value={technician.abbreviation}>
                          {technician.name}
                        </option>
                      );
                    })}
                  </select>
                </div> */}
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );

                  }
export default ServiceAppointment;
