import {useState, useEffect} from 'react';
import {NavLink} from 'react-router-dom';


function SalesList() {
    const [sales, setSales]  = useState([])
    const [salesPeople, setSalesPeople] = useState([])
    const [employeeNumber, setNumber] = useState('')

    const getSalesPeople = async () => {
      const url = await fetch('http://localhost:8090/api/salespeople/')
      const data = await url.json()
      setSalesPeople(data.sales_person)
    }


    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/sales/')
        const data = await resp.json()

        setSales(data.sales)
    }

    const handleChange = (e) => {
      setNumber(e.target.value)
    }


    useEffect(()=> {
        getData();
        getSalesPeople();
    }, [])


    return (
        <div className="container">
          <h1>Sales History</h1>
          <select onChange={handleChange} placeholder="Sales Person" required type="text" name="sales_person" id="sales_person" className="form-select">
            <option value=''>Sales Person</option>
            {salesPeople.map(sales_person => {
              return(
                <option key={sales_person.employee_number} value={sales_person.employee_number}>{sales_person.sales_person_name}</option>
              )
            })}
          </select>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Employee Name</th>
                <th>Employee ID</th>
                <th>Customer Name</th>
                <th>VIN</th>
                <th>Price ($USD)</th>
              </tr>
            </thead>
            <tbody>
              {sales.map(sale => { if(sale.sales_person.employee_number === employeeNumber || employeeNumber === "")
                return (
                  <tr key={sale.id}>
                    <td>{ sale.sales_person.sales_person_name }</td>
                    <td>{ sale.sales_person.employee_number }</td>
                    <td>{ sale.customer.customer_name }</td>
                    <td>{ sale.vin.vin }</td>
                    <td>{ sale.sales_price }</td>
                    <td><button onClick={async () => {
                      const resp = await fetch(`http://localhost:8090/api/sales/${sale.pk}/`, { method:"DELETE"})
                      const data = await resp.json()
                      getData()
                      }}>Delete</button></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div>
            <NavLink className="nav-link" aria-current="page" to="new"><button>Add a sale</button></NavLink>
          </div>
        </div>

        );
      }

  export default SalesList

