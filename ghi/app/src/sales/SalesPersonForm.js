import React from 'react';

class SalesPersonForm extends React.Component {
    state = {
        sales_person_name: "",
        employee_number: "",
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response =  await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales_person: data.sales_person });
        }
    }

    handleChangeInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    }


    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            sales_person_name: this.state.sales_person_name,
            employee_number: this.state.employee_number,
        }

        const sales_person_url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(sales_person_url, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();

            this.setState({
                sales_person_name: '',
                employee_number: '',
            });
        }


    }

    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create Sales Person</h1>
                  <form onSubmit={this.handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                      <input value={this.state.sales_person_name} onChange={this.handleChangeInput} placeholder="sales person name" required type="text" name="sales_person_name" id="sales_person_name" className="form-control"/>
                      <label htmlFor="sales_person_name">Sales Person Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={this.state.employee_number} onChange={this.handleChangeInput} placeholder="employee_number" required type="text" name="employee_number" id="employee_number" className="form-control"/>
                      <label htmlFor="employee_number">Employee #</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
          );
        }
}

export default SalesPersonForm
