import React from "react"

class AutomobileForm extends React.Component {

    state = {
        color: "",
        year: "",
        vin: "",
        models: [],
        model_id: "",
    }

    handleInputChange = (event) => {
        const value = event.target.value
        this.setState({[event.target.id]: value})
    }

    handleSubmit = async(event) => {
        event.preventDefault()
        const data = {...this.state}
        console.log(data)
        delete data.models
        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(autoUrl, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json()
            console.log(newAutomobile)

            const cleared = {
                color: "",
                year: "",
                vin: "",
                model_id: "",
            }
            this.setState(cleared)
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({models: data.models})
        }
    }
    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Automobile</h1>
                    <form onSubmit={this.handleSubmit} id="create-auto-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.color} onChange={this.handleInputChange} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.year} onChange={this.handleInputChange} placeholder="year" name="year" type="text" id="year" className="form-control"/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.vin} onChange={this.handleInputChange} placeholder="vin" name="vin" type="text" id="vin" className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleInputChange} value={this.state.model_id} required id="model_id" name="model_id" className="form-select">
                                <option value="">Choose a Model</option>
                                {this.state.models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>
                                    {model.id}
                                    </option>
                                )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}

export default AutomobileForm
